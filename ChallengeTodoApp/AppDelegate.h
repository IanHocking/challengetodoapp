//
//  AppDelegate.h
//  ChallengeTodoApp
//
//  Created by ian.user on 24/09/2020.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSTableViewDelegate>
{
    NSMutableArray *_todos;
}
@property (weak) IBOutlet NSTextField *textField;
@property (weak) IBOutlet NSTableView *tableView;

- (IBAction)buttonAddTodo:(id)sender;

@end

