//
//  AppDelegate.m
//  ChallengeTodoApp
//
//  Created by ian.user on 24/09/2020.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (strong) IBOutlet NSWindow *window;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application

}

- (id)init
{
    self = [super init];
    if (self) {

        // Logs can help the beginner understand what
        // is happening and hunt down bugs
        NSLog(@"init");

        // Initialise the array of todos
        _todos = [NSMutableArray arrayWithCapacity:1];

        // Become the delegate of the table view
        [_tableView setDelegate:self];

    }

    return self;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


- (IBAction)buttonAddTodo:(id)sender {

    NSString *text = [_textField stringValue];

    [_todos addObject:text];

    NSLog(@"todos: %@", _todos);

}

// Troubleshooting: What selectors are being called on me
// as a delegate?
- (BOOL)respondsToSelector:(SEL)aSelector
{
    NSString *methodName = NSStringFromSelector(aSelector);
    NSLog(@"respondsToSelector:%@", methodName);
    return [super respondsToSelector:aSelector];
}


// Table View delegate functions

// Size of vie
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tv
{
    NSLog(@"numberOfRowsInTableView called");
    return(NSInteger)[_todos count];
}

// Keith: This method is not executing at all, it seems,
// and yet the respondsToSelector: method output tells me that
// the NSTableView instance is indeed trying to call it. Any
// ideas?

// Todos to put in view
- (id)tableView:(NSTableView *)tv objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSString *v = [_todos objectAtIndex:row];
    NSLog(@"objectValueForTableColumn f called");
    return v;
}

@end
